package api.entities;

import java.util.*;

public class DatabaseSingleton {
    private static DatabaseSingleton db = null;

    private Map<Keyspace, List<AbstractTable>> keyspaces = new HashMap<>();

    private DatabaseSingleton() {
        Keyspace recordStudioKeyspace = new Keyspace("RecordStudio",
                "CREATE KEYSPACE IF NOT EXISTS RecordStudio WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};");
        List<AbstractTable> recordStudioTables = new ArrayList<>();
        recordStudioTables.add(new GradesTable(recordStudioKeyspace.getName()));
        this.keyspaces.put( recordStudioKeyspace, recordStudioTables);
    }

    public Set<Keyspace> getKeyspaces(){
        return keyspaces.keySet();
    }

    public List<AbstractTable> getTables(Keyspace keyspace){
        return keyspaces.get(keyspace);
    }

    public AbstractTable getTable(String keyspace_name, String table_name){
        List<AbstractTable> list = null;
        for (Keyspace keyspace : keyspaces.keySet()){
            if (keyspace.getName().equalsIgnoreCase(keyspace_name))
                list = keyspaces.get(keyspace);
        }
        if (list == null)
            return null;
        for (AbstractTable table : list){
            if (table.getName().equalsIgnoreCase(table_name))
                return table;
        }
        return null;
    }

    public static DatabaseSingleton getInstance() {
        if (db == null)
            db = new DatabaseSingleton();
        return db;
    }
}

package api;

import api.entities.AbstractTable;
import api.entities.DatabaseSingleton;
import com.datastax.driver.core.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputExecutor {
    public void start(Session session) {
        help();
        System.out.print("Please, enter your commands:\n$");
        try (Scanner sc = new Scanner(System.in)) {
            out:
            while (sc.hasNext()) {
                String line = sc.nextLine();
                String regex = "\"([^\"]*)\"|(\\S+)";
                List<String> params = new ArrayList<>();
                Matcher m = Pattern.compile(regex).matcher(line);
                while (m.find()) {
                    if (m.group(1) != null) {
                        params.add(m.group(1));
                    } else {
                        params.add(m.group(2));
                    }
                }
                String[] input = params.toArray(new String[0]);
                switch (input[0]) {
                    case "select":
                    case "update":
                    case "insert":
                    case "delete":
                    case "special":
                        try {
                            execute(session, input);
                        } catch (Exception e){
                            System.out.println("Some problems while executing: " + e);
                        }
                        break;
                    case "help":
                        help();
                        break;
                    case "exit":
                        break out;
                    default:
                        System.out.println("Unknown command.");
                        break;
                }
                System.out.print("$");
            }
        }
    }

    public void execute(Session session, String[] params) {
        final String command = params[0];
        if (params.length < 3){
            System.out.println("Not enough arguments to execute command.");
            return;
        }
        final DatabaseSingleton database = DatabaseSingleton.getInstance();
        final String keyspace_name = params[1];
        final String table_name = params[2];
        final AbstractTable table = database.getTable(keyspace_name, table_name);
        if (table == null){
            System.out.println("Table " + keyspace_name + " " + table_name + " doesn't exist.");
            return;
        }
        switch (command) {
            case "insert":
            case "update":
                table.insert_row(session, Arrays.copyOfRange(params, 3, params.length));
                break;
            case "select":
                table.select_rows(session, Arrays.copyOfRange(params, 3, params.length));
                break;
            case "delete":
                table.delete_row(session, Arrays.copyOfRange(params, 3, params.length));
                break;
            case "special":
                table.special(session, Arrays.copyOfRange(params, 3, params.length));
                break;
        }
    }

    public void help() {
        System.out.println("Input Executor.");
        System.out.println("Help:");
        System.out.println("\t[ (command params) | help | exit ]");
        System.out.println("\tcommand: select, update, insert, delete or special");
        System.out.println("\tparams: [keyspace table] and query params");
    }
}

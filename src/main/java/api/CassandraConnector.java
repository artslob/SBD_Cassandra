package api;

import com.datastax.driver.core.*;

import java.io.Closeable;

public class CassandraConnector implements Closeable {
    private final String DEFAULT_ADDRESS = "127.0.0.1";
    private final int DEFAULT_PORT = 9042;

    private Cluster cluster = null;
    private Session session = null;

    public void connect(){
        connect(DEFAULT_ADDRESS, DEFAULT_PORT);
    }

    public void connect(final String node, final int port) {
        cluster = Cluster.builder().addContactPoint(node).withPort(port).build();
        final Metadata metadata = cluster.getMetadata();
        System.out.println("Connected to cluster: " + metadata.getClusterName());
        for (final Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n",
                    host.getDatacenter(), host.getAddress(), host.getRack());
        }
        session = cluster.connect();
    }

    public Session getSession() {
        return this.session;
    }

    public void close() {
        session.close();
        cluster.close();
        System.out.println("Connection closed successfully.");
    }
}
